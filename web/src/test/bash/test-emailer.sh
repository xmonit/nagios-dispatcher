#!/bin/bash

#export DISPATCHER_HOST="10.156.67.49"
#export DISPATCHER_PORT="8080"
#export DISPATCHER_CONTEXT=""

#EMAIL_CMD=/usr/local/icinga/libexec/dispatch_email.py
EMAIL_CMD=../../main/python/dispatch_email.py

#This test does not use Nagios-Dispatcher but not sure where it should live (it uses dispatch_email.py)

for i in {1..10}
do
for EMAIL in "pkcinna@xmonit.com"
  do
  export ICINGA_CONTACTEMAIL="$EMAIL"
  export ICINGA_HOSTNAME="prd.xmonit.com"
  export ICINGA_HOSTALIAS="alias prd 01"
  export ICINGA_SERVICEDESC="ACE - Check getreport"
  export ICINGA_SERVICESTATE='OK'
  export ICINGA_SERVICEGROUPNAMES='ABC,DEF'
  export ICINGA_TIME=12:00:01
  export ICINGA_LASTSERVICECHECK=1409342970
  export ICINGA_SERVICEOUTPUT="HTTP OK: HTTP/1.1 200 OK - 10147 bytes in 0.099 second response time"
  export ICINGA_NOTIFICATIONTYPE='RECOVERY'
  $EMAIL_CMD service

  export ICINGA_CONTACTEMAIL="$EMAIL"
  export ICINGA_HOSTNAME="prd.xmonit.com"
  export ICINGA_HOSTALIAS="alias prd 01"
  export ICINGA_SERVICEDESC="Current Load"
  export ICINGA_SERVICEGROUPNAMES='ABC,DEF'
  export ICINGA_TIME=12:00:02
  export ICINGA_LASTSERVICECHECK=1409342971
  export ICINGA_HOSTSTATE='OK'
  export ICINGA_SERVICESTATE='OK'
  export ICINGA_HOSTOUTPUT="OK - load average: 0.93, 0.88, 0.87"
  export ICINGA_NOTIFICATIONTYPE='RECOVERY'
  $EMAIL_CMD service&

  export ICINGA_CONTACTEMAIL="$EMAIL"
  export ICINGA_HOSTNAME="prd.xmonit.com"
  export ICINGA_HOSTALIAS="alias prdx01"
  export ICINGA_SERVICEDESC="Heap - PROD ManagedInstance 01"
  export ICINGA_SERVICEGROUPNAMES='ABC,DEF,PROD XYZ,PROD ABC'
  export ICINGA_SERVICENOTESURL=""
  export ICINGA_TIME=12:00:03
  export ICINGA_LASTSERVICECHECK=1409342993
  export ICINGA_SERVICEACTIONURL=""
  export ICINGA_SERVICESTATE='OK'
  export ICINGA_SERVICEOUTPUT="JMX OK - java.lang:type=Memory.HeapMemoryUsage.used:406.01MB (max:1024.0MB free:617.99MB 60.35%) "
  export ICINGA_NOTIFICATIONTYPE='RECOVERY'
  $EMAIL_CMD&
  
  export ICINGA_CONTACTEMAIL="$EMAIL" 
  export ICINGA_HOSTNAME="prd.xmonit.com"
  export ICINGA_HOSTALIAS="alias prdx01"
  export ICINGA_SERVICEDESC="Heap - PROD ManagedInstance 01"
  export ICINGA_SERVICEGROUPNAMES='ABC,DEF'
  export ICINGA_TIME=12:00:04 
  export ICINGA_LASTSERVICECHECK=1409343974
  export ICINGA_SERVICENOTESURL="" 
  export ICINGA_SERVICEACTIONURL="" 
  export ICINGA_SERVICESTATE='WARNING' 
  export ICINGA_SERVICEOUTPUT="JMX ERROR - java.lang:type=Memory.HeapMemoryUsage.used:406.01MB (max:1024.0MB free:617.99MB 60.35%) " 
  export ICINGA_NOTIFICATIONTYPE='PROBLEM' 
  $EMAIL_CMD&
  
  export ICINGA_CONTACTEMAIL="$EMAIL" 
  export ICINGA_HOSTNAME="devx01"
  export ICINGA_HOSTALIAS="alias devx01"
  export ICINGA_SERVICEDESC="DEV01 Jenkins jenkins/login"
  export ICINGA_SERVICEGROUPNAMES='ABC,DEF'
  export ICINGA_SERVICESTATE='OK'
  export ICINGA_TIME=12:00:05 
  export ICINGA_LASTSERVICECHECK=1409343990
  export ICINGA_SERVICEOUTPUT="OK Found login token (0)" 
  export ICINGA_SERVICENOTESURL="http://devops.xmonit/icinga/check_service/notes.php?service_category=devops&env=devx01&service=Jenkens&method=login"
  export ICINGA_SERVICEACTIONURL="/pnp4icinga/graph?host=devx01&srv=DEVX01%20devops%20Jenkins/login"
  export ICINGA_NOTIFICATIONTYPE='RECOVERY' 
  $EMAIL_CMD&
  
  export ICINGA_CONTACTEMAIL="$EMAIL" 
  export ICINGA_HOSTNAME="prd.xmonit.com"
  export ICINGA_HOSTALIAS="alias prdx01"
  export ICINGA_TIME=12:00:06 
  export ICINGA_LASTSERVICECHECK=1409342976
  export ICINGA_SERVICEDESC="Heap - PROD ManagedInstance 01"
  export ICINGA_SERVICEGROUPNAMES='ABC,DEF'
  export ICINGA_SERVICENOTESURL="" 
  export ICINGA_SERVICEACTIONURL="" 
  export ICINGA_SERVICESTATE='WARNING' 
  export ICINGA_SERVICEOUTPUT="JMX ERROR2 - java.lang:type=Memory.HeapMemoryUsage.used:406.01MB (max:1024.0MB free:617.99MB 60.35%) " 
  export ICINGA_NOTIFICATIONTYPE='PROBLEM' 
  $EMAIL_CMD&

  done
done
