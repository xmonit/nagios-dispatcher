#!/bin/bash

#check_jmx!10999!-u monitorRole --password=@Password -o java.lang:type=Memory -a HeapMemoryUsage -k used -w 90% -c 95% --divisor 1048576 --units=MB

DISPATCHER_URL=https://localhost:8094/nagios-dispatcher


./dispatch  \
--serviceUrl=service:jmx:rmi:///jndi/rmi://test1.xmonit.com:10999/jmxrmi \
--user=monitorRole \
--password='@test1' \
--objectName='java.lang:type=Memory' \
--attrName=HeapMemoryUsage \
--attrKey=used \
--warningValue=90% \
--criticalValue=95% \
--units=MB \
--divisor=1048576 \
$DISPATCHER_URL/jmx/check

./dispatch  \
-s service:jmx:rmi:///jndi/rmi://test1.xmonit.com:10999/jmxrmi \
-u monitorRole \
-p '@test1' \
-o 'java.lang:type=Memory' \
-a HeapMemoryUsage \
-k used \
-w 90% \
-c 95% \
--units=MB \
--divisor=1048576 \
$DISPATCHER_URL/jmx/check
