import org.eclipse.jetty.server.Server
import org.eclipse.jetty.webapp.WebAppContext

object Jetty {

  def main(args: Array[String]) {

    val server = new Server(8091)
    val webapp = new WebAppContext()
    webapp.setContextPath("/")
    webapp.setWar("src/main/webapp")
    server.setHandler(webapp)
    server.start();
    server.join();
  }

}