/*
 * An efficient and isolated way to push icinga or nagios jmx calls to jetty server that is dedicated to
 * checks requiring running jvm
 *
 * From this cpp src dir: g++ -o ../../../bin/dispatch NagiosDispatchClient.cpp -lPocoNet
 */
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/StreamCopier.h>
#include <Poco/URI.h>
#include <Poco/Exception.h>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace Poco::Net;
using namespace Poco;
using namespace std;

enum NagiosStatus
{
  OK, WARNING, CRITICAL, UNKNOWN
};

const char * strStatus(NagiosStatus status)
{
	switch (status)
	{
	case OK:
	  return "OK";
	case WARNING:
	  return "WARNING";
	case CRITICAL:
	  return "CRITICAL";
	default:
	  return "UNKNOWN";
	}
}

class NagiosResult
{
  public:
    NagiosStatus status;
    string msg;

    NagiosResult() :
        status(UNKNOWN), msg("")
    {
    }

    void init(NagiosStatus status, istream& is)
    {
      this->status = status;
      stringstream respStringStream;
      StreamCopier::copyStream(is, respStringStream);
      msg = respStringStream.str();
      if (msg.size() > 255)
        msg = msg.substr(0, 255);
    }

    const char* strStatus()
    {
    	return ::strStatus(status);
    }

    string str()
    {
      return msg;
      //string rtn(strStatus());
      //rtn += " ";
      //rtn += msg;
      //return rtn;
    }
};

string RESERVED_QUERY_CHARS("?#=&+");

inline void addParam(const string& key, const string& val, string& query)
{
  if (!query.empty())
    query.append(1, '&');
  URI::encode(key, RESERVED_QUERY_CHARS, query);
  query.append(1, '=');
  URI::encode(val, RESERVED_QUERY_CHARS, query);
}

int main(int argc, char **argv)
{
  NagiosResult result;
  URI uri(argv[argc-1]);

  try
  {
    string scheme = uri.getScheme();
    if ( scheme != "http" )
    {
      throw std::runtime_error(string("Currently only http is supported for dispatcher.  Found: ") + uri.getScheme() );
    }

    string query(uri.getRawQuery());

    for( int i = 1; i < argc-1; i++ )
    {
      string arg(argv[i]);
      size_t len = arg.size();
      if ( len > 1 && arg[0] == '-' )
      {
        if ( arg[1] != '-' )
        {
          // Treat these cases as boolean flags:
          //    single dash with more than one char means multiple true booleans
          //    single dash but at end of args means true boolean,
          //    single dash and single char but next arg starts with dash
          if ( len > 2 || i == argc-2 || argv[i+1][0] == '-' )        
          {
            string shortArgs(arg.substr(1));
            for( string::iterator it = shortArgs.begin(); it != shortArgs.end(); it++)
            {
              char c = *it;
              if ( isalpha(c) )
                addParam(string("")+c,string("true"),query);
            }
          }
          else
          {
            string key(arg.substr(1));
            string val(argv[++i]);
            addParam(key,val,query);
          }
        }
        else if ( len > 2 )
        {
          string keyAndValue(arg.substr(2));
          size_t pos = keyAndValue.find("=");
          if ( pos == string::npos )
          {     
            if ( i == argc-2 ) 
            {      
              addParam(keyAndValue,"true",query);
            }
            else 
            {
              // dash dash followed by word implies next arg is the value
              // this means we do not support dash dash word to denote a boolean unless last arg
              string key(keyAndValue);
              string val(argv[++i]);
              addParam(key,val,query);
            }
          }
          else
          {          
            string key(keyAndValue.substr(0,pos));
            string val(keyAndValue.substr(pos+1));
            addParam(key,val,query);
          }
        }
        // this will skip "--" by itself
      }
      else
      {
        throw std::runtime_error(
          string("Pass through args must be followed by destination dispatcher URL.  Invalid arg: ") + arg);
      }
    }

    uri.setRawQuery(query);

    HTTPClientSession session(uri.getHost(), uri.getPort());
    HTTPRequest req;
    req.setMethod(HTTPRequest::HTTP_GET);
    req.setURI(uri.toString());
    session.sendRequest(req);

    HTTPResponse resp;
    istream &is = session.receiveResponse(resp);

    switch (resp.getStatus())
    {
    case 200:
      result.init(OK,is);
      break;
    case 201:
      result.init(WARNING,is);
      break;
    case 202:
      result.init(CRITICAL,is);
      break;
    case 203:
      result.init(UNKNOWN,is);
      break;
    default:
      stringstream s;
      s << "UNKNOWN - Dispatcher returned HTTP status: " << resp.getReason() << " (" << resp.getStatus() << ") ";
      result.init(UNKNOWN,s);
    }
  }
  catch (Exception const &ex)
  {
    stringstream s;
    s << "CRITICAL - " << ex.displayText();
    result.init(CRITICAL, s);
  }
  catch (std::exception const &ex)
  {
    stringstream s;
    s << "CRITICAL - " << ex.what();
    result.init(CRITICAL, s);
  }
  catch (...)
  {
    stringstream s;
    s << "CRITICAL - " << argv[0] << " unhandled exception";
    result.init(CRITICAL, s);
  }

  cout << result.str() << endl;
  return result.status;
}
