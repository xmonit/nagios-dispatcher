
import com.xmonit.dispatcher.email.EmailThread
import com.xmonit.dispatcher.nagios.AlertGroupManager
import com.xmonit.dispatcher.servlet._
import org.scalatra._
import javax.servlet.ServletContext


object ScalatraBootstrap {

  class AppInitializer {
    EmailThread.start()
    AlertGroupManager.init()
  }
}

class ScalatraBootstrap extends LifeCycle {

  override def init(context: ServletContext) {

    System.setProperty("scalate.workdir","work/scalate/nagios-dispatch")
    context.mount(new JmxServlet, "/jmx/*")
    context.mount(new JAuthServlet, "/jauth/*")
    context.mount(new NagiosQueryServlet, "/status/*")
    context.mount(new AdminServlet, "/admin/rest/*")
    context.mount(new EmailServlet, "/email/*")
  }

  override def destroy(context : javax.servlet.ServletContext){

    EmailThread.signalStop()
    AlertGroupManager.signalStop()
  }
}
