package com.xmonit.dispatcher.servlet


import org.scalatra._
import org.slf4j.LoggerFactory
import com.xmonit.dispatcher.nagios.{Alert, HostAlert, SvcAlert, AlertGroupManager}
import scala.collection.JavaConversions._


object EmailServlet {

  final val Log = LoggerFactory.getLogger(classOf[EmailServlet])

}

//
// Servlet class - Handles email urls for nagios notifications
//
class EmailServlet extends ScalatraServlet {

  final val Log = LoggerFactory.getLogger(classOf[EmailServlet])

  post("/hostAlert")(receiveHostAlert())

  post("/serviceAlert")(receiveServiceAlert())


  def receiveHostAlert() {
    try {
      val hostName = Alert.getParam(params,"HOSTNAME")

      val alertGroups = AlertGroupManager.hostInstance

      alertGroups.synchronized {

        var alert = HostAlert.alertsByHost.get(hostName)

        if ( alert != null ) {
          alert.update(params)
        } else {
          alert = new HostAlert(params)
          HostAlert.alertsByHost.put(hostName, alert)
          alertGroups.find { _.acceptAlert(alert) }.map { _.add(alert) }
        }
        alertGroups.notifyAll()
      }

    } catch {
      case throwable:Throwable => Log.error("Failed scheduling service alert",throwable)
    }
  }


  def receiveServiceAlert() {
    try {
      val hostName = Alert.getParam(params,"HOSTNAME")
      val svcDesc = Alert.getParam(params,"SERVICEDESC")

      val alertGroups = AlertGroupManager.svcInstance

      alertGroups.synchronized {

        var alert = SvcAlert.alertsByHostAndDesc.get((hostName,svcDesc))

        if ( alert != null ) {
          alert.update(params)
        } else {
          alert = new SvcAlert(params)
          SvcAlert.alertsByHostAndDesc.put((hostName, svcDesc), alert)
          alertGroups.find { _.acceptAlert(alert) }.map { _.add(alert) }
        }
        alertGroups.notifyAll()
      }

    } catch {
      case throwable:Throwable => Log.error("Failed scheduling service alert",throwable)
    }
  }
}
