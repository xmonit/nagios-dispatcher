package com.xmonit.dispatcher.servlet

import java.net.{URL, HttpURLConnection}

import org.json4s.{DefaultFormats, Formats}
import org.scalatra._

import org.apache.http.NameValuePair
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.{HttpGet, HttpPost}
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.message.BasicNameValuePair
import org.apache.http.util.EntityUtils
import org.scalatra.json.JacksonJsonSupport
import xml.XML
import com.xmonit.dispatcher.{RequestPolicy, AppConfig}
import org.slf4j.LoggerFactory

class JAuthServlet extends ScalatraServlet  with JacksonJsonSupport {

  final val Log = LoggerFactory.getLogger(classOf[ScalatraServlet])

  case class AuthResult(code: Int, message: String) {}

  case class AuthSession(contextUrl: String,
                         loginUrl: String,
                         loginSubmitUrl: String,
                         secureUrl: Option[String],
                         regEx: Option[String],
                         userConfigKey: String) {

    def validate() = {

      val httpclient = HttpClientBuilder.create().build();

      def openLoginPage() = {
        var resp = httpclient.execute(new HttpGet(loginUrl))
        var entity = resp.getEntity
        EntityUtils.consume(entity)
        if (resp.getStatusLine.getStatusCode != 200) {
          throw new Exception("Failed loading login page: " + loginUrl + ". " +
            resp.getStatusLine.toString)
        }
      }

      def submitLogin() = {
        val nvps = new java.util.ArrayList[NameValuePair]()
        val user = AppConfig.instance.getProperty(userConfigKey + ".user")
        if (user == null) {
          throw new Exception("No configuration value in NagiosDispatcher found for '" + userConfigKey + ".user'.")
        }
        val passwd = AppConfig.instance.getEncryptedProperty(userConfigKey + ".password")
        nvps.add(new BasicNameValuePair("j_username", user))
        nvps.add(new BasicNameValuePair("j_password", passwd))
        val httpPost = new HttpPost(loginSubmitUrl)
        httpPost.setHeader("Content-Type","application/x-www-form-urlencoded")
        httpPost.setEntity(new UrlEncodedFormEntity(nvps))
        val resp = httpclient.execute(httpPost)
        if ( !Seq(200,302).contains(resp.getStatusLine.getStatusCode) ) {
          throw new Exception("login submit failed. " + resp.getStatusLine.toString)
        }
        EntityUtils.toString(resp.getEntity)
      }

      def openSecurePage() = {
        val resp = httpclient.execute( new HttpGet(secureUrl.get) )
        if (resp.getStatusLine.getStatusCode != 200) {
          throw new Exception("Failed loading secured page: " + secureUrl + ". " + resp.getStatusLine.toString)
        }
        EntityUtils.toString(resp.getEntity)
      }

      val connPolicy = RequestPolicy.getRequestPolicy(contextUrl)

      try {
        connPolicy.beginRequest()
        openLoginPage()
        var content = submitLogin()
        if (secureUrl.isDefined) {
          content = openSecurePage()
        }
        if (regEx.isDefined && !content.matches(regEx.get)) {
          throw new Exception("Regular expression validation failed")
        }
        connPolicy.endRequestOk()
        AuthResult(0, "Login successful")
      } catch {
        case throwable: Throwable => {
          connPolicy.endRequestFail()
          throwable.printStackTrace()
          AuthResult(-4, throwable.getMessage)
        }
      } finally {
        httpclient.close()
      }
    }
  }

  protected implicit val jsonFormats: Formats = DefaultFormats


  post("/login") {

    contentType = "xml"
    val xmlBody = XML.loadString(request.body)
    val xmlReq = xmlBody \\ "Envelope" \ "Body" \ "JAuthRequest"

    val authResp = try {
      val contextUrl = (xmlReq \\ "contextUrl").text
      val loginUrl = contextUrl + (xmlReq \\ "loginPath").text
      val loginSubmitUrl = contextUrl + (xmlReq \\ "loginSubmitPath").text
      val secureUrl = (xmlReq \\ "securePath").headOption.map(contextUrl + _.text)
      val regEx = (xmlReq \\ "regEx").headOption.map(_.text)
      val userConfigKey = (xmlReq \\ "userConfigKey").text
      AuthSession(contextUrl, loginUrl, loginSubmitUrl, secureUrl, regEx, userConfigKey).validate()
    } catch {
      case throwable: Throwable => AuthResult(-1, throwable.getMessage)
    }

    Ok(
      <soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'>
        <soapenv:Body>
          <CheckSomeServiceResponse>
            <status>
              <code>{authResp.code}</code>
              <message>{authResp.message}</message>
            </status>
          </CheckSomeServiceResponse>
        </soapenv:Body>
      </soapenv:Envelope>)
  }
}
