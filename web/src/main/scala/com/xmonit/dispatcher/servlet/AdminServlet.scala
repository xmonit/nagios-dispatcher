package com.xmonit.dispatcher.servlet

import org.scalatra._
import json.JacksonJsonSupport
import org.json4s._
import com.xmonit.dispatcher.{RequestPolicy, AppConfig}
import AppConfig.{instance => app }
import scala.collection.JavaConverters._

case class KeyValue(key:String,value:String)

class AdminServlet extends ScalatraServlet with JacksonJsonSupport {

  protected implicit val jsonFormats: Formats = DefaultFormats

  case class AdminResult(status:String,statusMsg:String)

  delete("/properties/:propName") {
    val msg = try {
      app.remove(params("propName"))
      app.save()
      "Removed " + params("propName")
    } catch {
      case t:Throwable => t.getMessage
    }
    Map( "statusMsg"-> msg, "props"-> app.asScala.toList.sortBy { _._1 }.map{ x => KeyValue(x._1,x._2)} )
  }

  get("/properties") {
    contentType = formats("json")
    import scala.collection.JavaConverters._
    val props = AppConfig.instance.asScala
    props.toList.sortBy { _._1 }.map{ x => KeyValue(x._1,x._2)}
  }

  post("/properties/:propName") {
    contentType = formats("json")
    val msg = try{
      val parsedBody = readJsonFromBody(request.body)
      val propName = params("propName")
      val args = parsedBody.extract[Map[String,String]]
      if (args.getOrElse("encrypt", "false").toBoolean) {
        app.setEncryptedProperty(propName, args("propValue"))
      } else {
        app.setProperty(propName, args("propValue"))
      }
      app.save()
      "Updated " + propName
    } catch {
      case t:Throwable => t.getMessage
    }
    Map( "statusMsg"-> msg, "props"-> app.asScala.toList.sortBy { _._1 }.map{ x => KeyValue(x._1,x._2)} )
  }

  post("/flushCache"){
    contentType = formats("txt")
    RequestPolicy.requestPolicyCache.flush()
    new java.util.Date().toString() + " - Cache flushed"
  }
}
