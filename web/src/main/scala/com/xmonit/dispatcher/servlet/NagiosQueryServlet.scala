package com.xmonit.dispatcher.servlet

import org.scalatra._
import java.io.FileReader
import json.JacksonJsonSupport
import com.xmonit.dispatcher.nagios.{NagiosService, NagiosStatusParser}
import com.xmonit.dispatcher.AppConfig
import org.json4s.{DefaultFormats, Formats}


class NagiosQueryServlet extends ScalatraServlet with JacksonJsonSupport {

  protected implicit val jsonFormats: Formats = DefaultFormats


  final val KEYS = Set("host_name","service_description","current_state","plugin_output",
    "last_check","last_time_ok","last_state_change")

  get("/:serviceFilter"){
    contentType = "json"
    val serviceFilter = params.getOrElse("serviceFilter",".*")
    val reader = new FileReader( AppConfig.instance.getProperty("nagios.statusFile") )
    val parser = new NagiosStatusParser()
    val result = parser.parse(reader)

    def applyServiceFilter(s:NagiosService) = {
      def hostAndService = s.dict("host_name") + ',' + s.dict("service_description")
      s.name == "servicestatus" && hostAndService.matches(serviceFilter)
    }

    val services = result.get.filter( applyServiceFilter(_) ).map { s =>
      NagiosService(s.name, s.dict.filterKeys(KEYS.contains(_)))
    }

    Ok(services)
  }
}
