package com.xmonit.dispatcher.servlet

import org.scalatra._
import javax.management.remote.JMXServiceURL
import com.xmonit.dispatcher.RequestPolicy
import com.xmonit.dispatcher.nagios.{NagiosResult,NagiosStatus}
import NagiosStatus._
import org.slf4j.LoggerFactory
import com.xmonit.dispatcher.jmx.{JmxClient, JmxRequest}


object JmxServlet {
  final val Log = LoggerFactory.getLogger(classOf[ScalatraServlet])
}


class JmxServlet extends ScalatraServlet {

  get("/check") {
    var result : NagiosResult = null
    try {
      val jmxRequest = JmxRequest(
        new JMXServiceURL(params.getOrElse("s", params("serviceUrl"))),
        params.getOrElse("o", params("objectName")),
        params.getOrElse("a", params("attrName")),
        params.getOrElse("k", params("attrKey")),
        params.get("weblogic").map(_.toBoolean).getOrElse(false),
        params.get("u").orElse( params.get("user") ),
        params.get("p").orElse( params.get("password") ),
        params.get("w").orElse( params.get("warningValue") ),
        params.get("c").orElse( params.get("criticalValue") ),
        params.get("operation"),
        params.getOrElse("units", ""),
        params.get("divisor").map(_.toDouble).getOrElse(1.0d),
        params.get("precision").map(_.toInt))
      import jmxRequest._

      val connPolicy = RequestPolicy.getRequestPolicy(serviceUrl.toString)

      connPolicy.beginRequest()

      val jmxClient = new JmxClient
      try {
        jmxClient.connect(serviceUrl, user, password, requireWeblogic)
        result = jmxClient.execute(jmxRequest)
        connPolicy.endRequestOk()
      } catch {
        case ex: Throwable =>
          connPolicy.endRequestFail()
          result = NagiosResult(CRITICAL, ex.getMessage)
      } finally {
        jmxClient.disconnect()
      }
    } catch {
      case ex: Throwable =>
        JmxServlet.Log.warn(ex.getMessage)
        result = NagiosResult(CRITICAL, ex.getMessage)
    }

    contentType = "txt"

    val respCode = result.status match {
      case OK => 200
      case WARNING => 201
      case CRITICAL => 202
      case _ =>  203
    }
    ActionResult(respCode, "JMX " + result.status + " - " + result.msg,Map())
  }
}
