package com.xmonit.dispatcher.jmx

import javax.management.{ObjectName, MBeanServerConnection}
import javax.management.remote.{JMXConnectorFactory, JMXServiceURL, JMXConnector}
import java.util
import com.xmonit.dispatcher.nagios.NagiosStatus._
import com.xmonit.dispatcher.nagios.NagiosResult
import scala.Some
import javax.management.openmbean.CompositeDataSupport
import com.xmonit.dispatcher.nagios
import org.slf4j.LoggerFactory
import collection.mutable.ListBuffer


object JmxClient {

  final val Log = LoggerFactory.getLogger(getClass)

}


class JmxClient {

  var mbeanServerConn: MBeanServerConnection = null

  var connector: JMXConnector = null


  def connect(serviceUrl: JMXServiceURL,
              user: Option[String],
              passwd: Option[String],
              requireWeblogic: Boolean) {
    val env: util.HashMap[String, Object] = new util.HashMap()
    if (requireWeblogic) {
      env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_PACKAGES, "weblogic.management.remote")
    }
    if (user.isDefined && passwd.isDefined) {
      env.put(JMXConnector.CREDENTIALS, Array(user.get, passwd.get))
    }
    connector = JMXConnectorFactory.connect(serviceUrl, env)
    mbeanServerConn = connector.getMBeanServerConnection
  }


  def disconnect() {
    if (connector != null)
      connector.close()
  }


  def execute(req: JmxRequest) = {
    import req._
    val startMs: Long = System.currentTimeMillis()

    val result = try {
      var objName = new ObjectName(objectName)
      if (objName.isPropertyPattern || objName.isDomainPattern) {
        def mBeans = mbeanServerConn.queryMBeans(objName, null)
        if (mBeans.size() != 1) {
          throw new Exception("Expected one result for '${objectName}' (found ${mbeans.size()}).")
        }
        else {
          objName = mBeans.iterator().next().getObjectName
        }
      }

      val value = getAttrValue(objName, attrName, attrKey)
      val maxOpt = value match {
        case n: Number if ("used" == attrKey) => {
          try {
            Some(getAttrValue(objName, attrName, "max").asInstanceOf[Number].doubleValue())
          } catch {
            case e: Exception => Some(n.doubleValue)
          }
        }
        case _ => None
      }

      if (operation.isDefined) {
        mbeanServerConn.invoke(objName, operation.get, null, null)
      }

      val attrPath = s"$objectName.$attrName.$attrKey"

      val ms = System.currentTimeMillis - startMs
      value match {
        case n: Number => getResult(attrPath, n.doubleValue, maxOpt, ms, req)
        case o: AnyRef => getResult(attrPath, o.toString, ms, req)
        case _ => getResult(attrPath, "", ms, req)
      }
    } catch {
      case ex: Exception =>
        JmxClient.Log.warn(ex.getMessage)
        NagiosResult(CRITICAL, ex.getClass.getSimpleName + " - " + ex.getMessage)
    } finally {
      if (connector != null)
        connector.close()
      connector = null
    }
    result
  }


  def getAttrValue(objectName: ObjectName, attrName: String, attrKey: String): Object = {
    val attr = mbeanServerConn.getAttribute(objectName, attrName)
    attr match {
      case cdsAttr: CompositeDataSupport => cdsAttr.get(attrKey)
      case _ => attr
    }
  }


  def getResult(attrPath: String, dValue: Double, maxOpt: Option[Double], ms: Long, req: JmxRequest) = {
    val precisionFmt = "%." + req.precision.getOrElse("2") + "f"
    val value = dValue / req.divisor
    val scaledMaxOpt = maxOpt.map(_ / req.divisor)
    val warnVal = req.warningValue.map(parseThresholdOption(_, scaledMaxOpt))
    val criticalVal = req.criticalValue.map(parseThresholdOption(_, scaledMaxOpt))

    var status = OK
    val warn = (warnVal match {
      case Some(v) =>
        if (value > v)
          status = WARNING
        String.format(precisionFmt, v.asInstanceOf[java.lang.Double] )
      case _ => ""
    })
    val critical = (criticalVal match {
      case Some(v) =>
        if (value > v)
          status = CRITICAL
        String.format(precisionFmt,  v.asInstanceOf[java.lang.Double])
      case _ => ""
    })
    val limits = List(warn,critical)

    val strVal = String.format(precisionFmt, value.asInstanceOf[java.lang.Double])
    var remaining = ""

    val strMax = scaledMaxOpt.map { m =>
      val strFree = String.format(precisionFmt, (m - value).asInstanceOf[java.lang.Double])
      val strPercent = String.format(precisionFmt, (100.0 * (1.0 - value / m)).asInstanceOf[java.lang.Double])
      remaining = s" (max:${m}${req.units} free:${strFree}${req.units} ${strPercent}%)"
      String.format(precisionFmt, m.asInstanceOf[java.lang.Double])
    }.getOrElse("")

    val msg = s"${attrPath}:${strVal}${req.units}${remaining}|${attrPath.replaceAll("[ =:.]", "_")}=" +
      s"${strVal}${req.units};${limits.mkString(";")};;${strMax}"
    NagiosResult(status, msg)
  }


  def getResult(attrPath: String, value: String, ms: Long, req: JmxRequest) = {
    var status = OK
    val limits = List(
      req.warningValue match {
        case Some(wv) => if (value.matches(wv)) status = WARNING; wv
        case _ => ""
      },
      req.criticalValue match {
        case Some(cv) => if (value.matches(cv)) status = CRITICAL; cv
        case _ => ""
      }
    )
    val fmtValue = req.precision.map(value.take(_)).getOrElse(value)

    nagios.NagiosResult(status, s"${attrPath}:{$value} ${req.units}|${attrPath}=${fmtValue}${req.units};${limits.mkString(";")};;")
  }


  def parseThresholdOption(value: String, maxOpt: Option[Double]) = {
    if ( value.endsWith("%") ) {
      val max = maxOpt.getOrElse(throw new Exception("Threshold is defined as percentage but no max is defined"))
      value.dropRight(1).toDouble * 100.0 * max
    } else {
      value.toDouble
    }
  }
}
