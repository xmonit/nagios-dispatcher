package com.xmonit.dispatcher.jmx

import javax.management.remote.JMXServiceURL

case class JmxRequest(
   serviceUrl: JMXServiceURL,
   objectName: String,
   attrName: String,
   attrKey: String,
   requireWeblogic: Boolean,
   user: Option[String],
   password: Option[String],
   warningValue: Option[String],
   criticalValue: Option[String],
   operation: Option[String],
   units: String,
   divisor: Double,
   precision: Option[Int]
)
