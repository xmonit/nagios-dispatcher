package com.xmonit.dispatcher.email

import java.util.concurrent.LinkedBlockingQueue

import org.slf4j.LoggerFactory
import com.xmonit.dispatcher.email.EmailThread._
import com.xmonit.dispatcher.nagios.AlertGroup

import scala.collection.JavaConversions._


object EmailThread {

  final val Log = LoggerFactory.getLogger(classOf[EmailThread])

  case class QueueEntry( emails: Set[String], alertGroup: AlertGroup )

  val instance = new EmailThread()

  def start() = instance.start()

  def signalStop() = instance.signalStop()
}


class EmailThread() extends Thread {


  val alertsToEmail = new LinkedBlockingQueue[QueueEntry]()


  def process( queueEntry: QueueEntry ) {

    import queueEntry._

    val (subject,body) = alertGroup.parent.expandTemplates( alertGroup )
    val altText = alertGroup.mkString("\r\n")

    Log.info(s"Emailing ${emails} for alerts ${alertGroup.toList}")
    try {
      Email.sendEmail(emails, subject, body, altText)
    } catch {
      case ex:Exception => {
        if ( emails.size > 1 ) {
          // one of the emails may be invalid so try sending individually
          Log.error("Failed sending emails as group, trying individually",ex)
          emails.foreach{ email =>
            try {
              Email.sendEmail(Set(email), subject, body, altText)
            } catch {
              case ex:Exception => Log.error("Failed sending email to '" + email + "' from group " + emails, ex)
            }
          }
        } else {
          throw ex
        }
      }
    }
  }


  def put( emails: Set[String], alertGroup: AlertGroup) = {
    alertsToEmail.put( EmailThread.QueueEntry(emails, alertGroup) )
  }


  def put( email: String, alertGroup: AlertGroup)  : Unit = put( Set(email), alertGroup)


  override def run() {

    var queueEntry : QueueEntry = alertsToEmail.take()

    while ( queueEntry != null ) {

      try {
        process(queueEntry)
      } catch {
        case throwable: Throwable => Log.error("Failed sending email to " + queueEntry.emails, throwable)
      }

      queueEntry = alertsToEmail.take()
    }
  }


  def signalStop() {
    alertsToEmail.put(null)
  }
}