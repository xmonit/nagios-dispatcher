package com.xmonit.dispatcher.email

import java.net.URL
import javax.mail.util.ByteArrayDataSource

import org.apache.commons.mail.{DefaultAuthenticator, ImageHtmlEmail}
import org.apache.commons.mail.resolver.DataSourceUrlResolver
import org.slf4j.LoggerFactory
import com.xmonit.dispatcher.AppConfig.{instance => app}


object Email {

  final val Log = LoggerFactory.getLogger(this.getClass)

  def sendEmail(toAddrs: Set[String], emailSubject: String, htmlBody: String, textBody: String) {

    val shadowAddrs = (app.getProperty("admin.shadowEmails") match {
      case se : String if !se.trim().isEmpty() => se.split( """\s*[,;]\s*""").toList
      case _ => List()
    }).filter{ !toAddrs.contains(_) }

    def newEmail(to:Seq[String]) = new ImageHtmlEmail() {
      val nagiosEmailUser = app.getProperty("nagios.email.user")
      if ( nagiosEmailUser != null ) {
        val passwd = app.getEncryptedProperty("nagios.email.password")
        setAuthenticator(new DefaultAuthenticator(nagiosEmailUser, passwd));
        //setSSLOnConnect(true);
      }
      val nagiosUrl = new URL(app.getRequiredProperty("nagios.hostUrl"))
      setDataSourceResolver(new DataSourceUrlResolver(nagiosUrl, true))
      setHostName("mail.xmonit.com")
      setSmtpPort(25)
      setFrom(app.getRequiredProperty("nagios.replyEmail"), "Icinga Notifier")
      setSubject(emailSubject)
      setHtmlMsg(htmlBody)
      setTextMsg(textBody)
      to.foreach { addTo }
      shadowAddrs.foreach( addBcc )
    }

    newEmail(toAddrs.toSeq).send()
    /*
    val shadowAddrs = (app.getProperty("admin.shadowEmails") match {
      case se : String if !se.trim().isEmpty() => se.split( """\s*[,;]\s*""").toList
      case _ => List()
    }).filter{ !toAddrs.contains(_) }
    try {
      if ( !shadowAddrs.isEmpty ) {
        val shadowEmail = newEmail( shadowAddrs )
        shadowEmail.attach(new ByteArrayDataSource("TO: " +  toAddrs.mkString(", "), "text/plain;"),"info.txt","original email info")
        shadowEmail.send()
        Log.info("Sent shadow email to " + shadowAddrs.mkString(", ") + " for TO list: " + toAddrs.mkString(", "))
      }
    } catch {
      case e : Exception => Log.warn("Failed sending shadow email to " + shadowAddrs, e);
    }
    */
  }
}
