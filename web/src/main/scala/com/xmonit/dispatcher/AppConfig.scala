package com.xmonit.dispatcher

import java.io.{FileInputStream, FileOutputStream, InputStream}

import javax.crypto.{Cipher, SecretKey}
import javax.crypto.spec.SecretKeySpec
import org.apache.commons.codec.binary.Base64
import org.slf4j.LoggerFactory
import org.springframework.security.core.userdetails.{UserDetailsService, UsernameNotFoundException}
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import com.xmonit.dispatcher.nagios.AlertGroupManager

import scala.collection.JavaConversions._
import java.util.{Collections, Enumeration, Properties}

import org.springframework.security.crypto.password.PasswordEncoder


object AppConfig {

  final val Log = LoggerFactory.getLogger(classOf[AppConfig])

  val key : SecretKey = new SecretKeySpec(Array[Byte](0x03, 0x14, 0x69, 0x01, 0x02, 0x04, 0x55, 0x05), "DES")
  val ecipher = Cipher.getInstance("DES")
  val dcipher = Cipher.getInstance("DES")

  ecipher.init(Cipher.ENCRYPT_MODE, key)
  dcipher.init(Cipher.DECRYPT_MODE, key)

  var instance : AppConfig = null
  var version = ""
  var buildTimeStamp = ""

  def decrypt(encryptedText:String) = new String(dcipher.doFinal(Base64.decodeBase64(encryptedText.getBytes)))
  def encrypt(plainText:String) = Base64.encodeBase64String(ecipher.doFinal(plainText.getBytes))
}
import AppConfig._



class AppConfig extends java.util.Properties with UserDetailsService {

  instance = this

  var GlobalMaxOpenRequests = 500
  var GlobalMaxOpenRequestsOnFail = 50
  var MaxRequestsOnOk = 20
  var MaxRequestsOnFail = 2

  try {
    val buildProps = new Properties()
    val is = classOf[AppConfig].getClassLoader.getResourceAsStream("app.properties")
    buildProps.load(is)
    is.close()
    version = buildProps.getProperty("maven.version")
    buildTimeStamp = buildProps.getProperty("maven.build.timestamp")
  } catch {
    case ex:Exception => ex.printStackTrace()
  }

  try {
    load(new FileInputStream("etc/nagios-dispatcher.properties"))
  } catch {
    case ex : Exception =>
      Log.error(ex.getMessage)
      setProperty("admin.password","""/TM/HxJxAIgGQbjejcIDHw\=\=\r\n""")
      setProperty("admin.shadowEmails","pkcinna@xmonit.com")
      setProperty("nagios.statusFile","/usr/local/icinga/var/status.dat")
      setProperty("nagios.hostUrl", "https://devops.xmonit.com")
      setProperty("nagios.url", "https://devops.xmonit.com/icinga/")
      setProperty("nagios.replyEmail", "mailer@xmonit.com")
      setProperty("requestPolicy.globalMaxOpenRequests","500")
      setProperty("requestPolicy.globalMaxOpenRequestsOnFail","50")
      setProperty("requestPolicy.maxRequestsOnOk","20")
      setProperty("requestPolicy.maxRequestsOnFail","2")
      save()
  }


  def getEncryptedProperty(key: String) = {
    val propVal = getProperty(key)
    if ( propVal == null ) {
      throw new Exception("No configuration value found for key:" + key)
    }
    decrypt(propVal)
  }


  def getRequiredProperty(key:String) = {
    if ( !containsKey(key) ) {
      throw new Exception("Required property missing: " + key )
    }
    getProperty(key)
  }


  override def keys() : Enumeration[Object] = {
    synchronized{
      return Collections.enumeration(new java.util.TreeSet(super.keySet())).asInstanceOf[Enumeration[Object]];
    }
  }


  override def load(is:InputStream) {
    super.load(is)
    updateCachedValues()
  }

  var passwordEncoder : PasswordEncoder = new org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder()

  def setPasswordEncoder( injectedEncoder : PasswordEncoder): Unit = {
    passwordEncoder = injectedEncoder
  }

  def loadUserByUsername(user: String) = {
    if ( user != "admin" )
      throw new UsernameNotFoundException("Invalid user or password!")
    val pass = passwordEncoder.encode(getEncryptedProperty("admin.password"))
    new User("admin", pass, Set(new SimpleGrantedAuthority("ROLE_ADMIN")))
  }

  def save() {
    this.synchronized {
      updateCachedValues()
      AlertGroupManager.restart()
      store(new FileOutputStream("etc/nagios-dispatcher.properties"),"")
    }
  }


  def setEncryptedProperty(key: String, plainText: String ) = setProperty(key, encrypt(plainText))


  def updateCachedValues(){
    try{
      GlobalMaxOpenRequests = getProperty("requestPolicy.globalMaxOpenRequests","500").toInt
      GlobalMaxOpenRequestsOnFail = getProperty("requestPolicy.globalMaxOpenRequestsOnFail","50").toInt
      MaxRequestsOnOk = getProperty("requestPolicy.maxRequestsOnOk","20").toInt
      MaxRequestsOnFail = getProperty("requestPolicy.maxRequestsOnFail","2").toInt
    } catch {
      case ex:Exception => ex.printStackTrace()
    }
  }

}

