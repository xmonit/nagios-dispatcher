package com.xmonit.dispatcher

import net.sf.ehcache.{Element, Cache}
import net.sf.ehcache.config.CacheConfiguration
import net.sf.ehcache.store.MemoryStoreEvictionPolicy
import com.xmonit.dispatcher.AppConfig.{instance=>conf}


class RequestPolicyException(msg:String) extends Exception(msg)


object RequestPolicy {

  private var openRequests = 0L

  val requestPolicyCache = new Cache(
    new CacheConfiguration("RequestPolicyCache", 1000)
      .memoryStoreEvictionPolicy(MemoryStoreEvictionPolicy.LFU)
      .timeToLiveSeconds(7*24*60*60)
      .timeToIdleSeconds(60*60)
      .overflowToOffHeap(false))

  CacheMgr.cacheMgr.addCache(requestPolicyCache)

  
  def beginGlobalRequest() {
    this.synchronized{
      if( openRequests >= conf.GlobalMaxOpenRequests )
        throw new RequestPolicyException("Exceeded global max open request count ("
          + conf.GlobalMaxOpenRequests + ") for badly implemented API's like JMX")
      openRequests += 1
    }
  }


  def endGlobalRequest() {
    this.synchronized{
      openRequests -= 1
    }
  }


  def beginGlobalRequestOnPriorFail() {
    this.synchronized{
      if ( openRequests >= conf.GlobalMaxOpenRequestsOnFail )
        throw new RequestPolicyException("Exceeded global max open request count for failing services ("
          + conf.GlobalMaxOpenRequestsOnFail + ")")
    }
  }


  def getRequestPolicy(category:String) : RequestPolicy = {
    this.synchronized {
      var element = requestPolicyCache.get(category)
      if ( element == null ){
        println("Creating new policy for " + category)
        val rtnPolicy = new RequestPolicy()
        element = new Element(category,rtnPolicy)
        requestPolicyCache.put(element)
        return rtnPolicy
      }
      return element.getObjectValue.asInstanceOf[RequestPolicy]
    }
  }
}
import RequestPolicy._



class RequestPolicy {

  var sameStateCnt = 0
  var openRequestCnt = 0
  var requestOk = true



  def beginRequest(){
    this.synchronized{
      if ( !requestOk ) {
        beginGlobalRequestOnPriorFail()
        if ( openRequestCnt >= conf.MaxRequestsOnFail )
          throw new RequestPolicyException("Rejected request because last " + sameStateCnt
            + " request(s) failed " +
          " and " + openRequestCnt + " request(s) are already open.")
      } else {
        if ( openRequestCnt >= conf.MaxRequestsOnOk )
          throw new RequestPolicyException("Rejected request because " + openRequestCnt
            + " request(s) are already open.")
      }
      beginGlobalRequest()
      openRequestCnt+=1
      //println(this)
    }
  }


  def endRequest(requestOk:Boolean) {
    this.synchronized{
      if ( requestOk == this.requestOk ) {
        sameStateCnt += 1
      } else {
        this.requestOk = requestOk
        sameStateCnt = 0
      }
      openRequestCnt -= 1
      endGlobalRequest()
    }
  }


  def endRequestOk() { endRequest(requestOk=true) }


  def endRequestFail() { endRequest(requestOk=false) }


  override def toString = {
    "Open:" + openRequestCnt + " ok:" + requestOk + " Total open:" + openRequests + " Total cached policies: " +
      requestPolicyCache.getMemoryStoreSize
  }
}
