package com.xmonit.dispatcher

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

object DateUtils {

  val formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")

  def addMilliSeconds(date:Date, ms:Int) = {
    val cal = Calendar.getInstance()
    cal.setTime(date)
    cal.add(Calendar.MILLISECOND, ms)
    cal.getTime
  }

  def addSeconds(date:Date, s:Int) = {
    val cal = Calendar.getInstance()
    cal.setTime(date)
    cal.add(Calendar.SECOND, s)
    cal.getTime
  }

  def addMinutes(date:Date, minutes:Int) = {
    val cal = Calendar.getInstance()
    cal.setTime(date)
    cal.add(Calendar.MINUTE, minutes)
    cal.getTime
  }

  def addHours(date:Date, hours:Int) = {
    val cal = Calendar.getInstance()
    cal.setTime(date)
    cal.add(Calendar.HOUR_OF_DAY, hours)
    cal.getTime
  }

  def addDays(date:Date, days:Int) = {
    val cal = Calendar.getInstance()
    cal.setTime(date)
    cal.add(Calendar.DATE, days)
    cal.getTime
  }
}
