package com.xmonit.dispatcher.nagios

import java.text.SimpleDateFormat

import org.fusesource.scalate.RenderContext
import com.xmonit.dispatcher.AppConfig
import com.xmonit.dispatcher.nagios.Alert.AlertHistory



object Alert {

  case class AlertHistory(state:String,timeStamp:String,output:String) {
    def time = formatTimeStamp(timeStamp)
  }

  val hhmmss = new SimpleDateFormat("HH:mm:ss")

  def formatTimeStamp(timeStamp:String) = hhmmss.format(new java.util.Date(new Integer(timeStamp) * 1000L))

  lazy val paramPrefix = AppConfig.instance.getProperty("nagios.paramPrefix","ICINGA_")

  def getParam(params:Map[String,String],paramKey:String) = params(getParamName(paramKey))

  def getParamName(paramKey:String) = paramPrefix + paramKey

  def highlight(context:RenderContext, str:String, strRegex:String, color:String="yellow") = {
    val highlightRegex = strRegex.r
    var matches = highlightRegex.findAllIn(str).matchData
    var offset = 0
    var end = 0
    for (m <- matches) {
      val start = m.start - offset
      if ( start > 0 ) {
        context <<< str.subSequence(offset,m.start)
      }
      context << s"<span style='background:$color'>${m.matched}</span>"
      offset = m.end
    }
    context <<< str.substring(offset,str.length())
  }
}



abstract class Alert( var params:Map[String,String],
                      val addTimeMs:Long = System.currentTimeMillis(),
                      var parent: AlertGroup = null ) {

  private val emails: java.util.Set[String] = java.util.Collections.synchronizedSet(new java.util.TreeSet())


  def getActionUrl(paramKey:String) = params.getOrElse( Alert.getParamName(paramKey),"").replaceAll("' class='tips.*$","") match {
    case url if url.startsWith("http") => url
    case path if !path.isEmpty => AppConfig.instance.getProperty("nagios.hostUrl") + path
    case _ => ""
  }


  def getParam(paramKey:String) = Alert.getParam(params,paramKey)


  def getParamOrElse(paramKey:String, default:String) = params.getOrElse( Alert.getParamName(paramKey),"")


  def key(params:Map[String,String]) : Tuple3[String,String,String]


  def remove()


  def updateEmails() = emails.add( getParam("CONTACTEMAIL") )


  def update(newParams:Map[String,String]) {
    this.synchronized {
      val newKey = key(newParams)
      val lastKey = key(params)
      import scala.collection.JavaConversions._
      if ( newKey != lastKey && !recentHistory.exists { h => (h.state,h.timeStamp,h.output) == lastKey }) {
        recentHistory.add(0, AlertHistory(state, timeStamp, output))
      }
      params = newParams
      updateEmails()
    }
  }

  // These need to be defined at HostAlert and ServiceAlert
  def desc : String
  def output : String
  def state : String
  def timeStamp : String
  def notesUrl : String
  def actionUrl : String

  def hostAlias = getParam("HOSTALIAS")
  def hostName = getParam("HOSTNAME")
  def notifyType = getParam("NOTIFICATIONTYPE")

  def time = Alert.formatTimeStamp(timeStamp)


  val recentHistory = java.util.Collections.synchronizedList(new java.util.LinkedList[AlertHistory]())


  override def toString = {
    state + " " +  hostName + " " + output
  }

  def withEmails( fn: (String) => Unit ) = emails.synchronized{
    import scala.collection.JavaConversions._
    emails.foreach( fn )
  }


  updateEmails()
}
