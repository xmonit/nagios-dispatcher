package com.xmonit.dispatcher.nagios

import com.xmonit.dispatcher.AppConfig.{instance => app}
import com.xmonit.dispatcher.email.EmailThread

import com.xmonit.dispatcher.nagios.AlertGroup.ParamMatcher

import scala.util.control.NonFatal
import scala.util.matching.Regex
import scala.collection.JavaConversions._


object AlertGroup {

  case class ParamMatcher( paramName: String, regEx: String ) extends Regex(regEx) {

    def this( paramConfigKey: String ) {
      this( app.getRequiredProperty( paramConfigKey + ".param"), app.getRequiredProperty( paramConfigKey + ".regex" ) )
      val strExpect = app.getProperty( paramConfigKey + ".expect")
      if ( strExpect != null )
        expect = strExpect.toBoolean
    }

    var expect = true

    def acceptAlert( alert: Alert ) : Boolean  = {
      alert.params.get(paramName) match {
        case Some(paramValue) => {
          val accepted = pattern.matcher(paramValue).matches()
          if ( !expect ) !accepted else accepted
        }
        case _ => false
      }
    }

    override def toString() = "Parameter: " + paramName + ", Regex: " + regEx + " Expect: " + expect
  }

}

case class AlertGroup( configKey: String,
                       title: String,
                       paramMatchers: Seq[ParamMatcher],
                       idleWaitSecs: Int,
                       maxWaitSecs: Int,
                       groupEmails: Boolean,
                       expect: Boolean ) extends java.util.Vector[Alert] {

  var parent: AlertGroupManager = null

  def this( configKey: String, parent: AlertGroupManager ) = {
    this( configKey,
      app.getProperty( configKey + ".title", configKey ),
      app.getProperty( configKey + ".matchers" ).split("""\s*,\s*""").toList.filter{ !_.isEmpty }.map{ new ParamMatcher(_) },
      app.getProperty( configKey + ".idleWaitSecs", "1").toInt,
      app.getProperty( configKey + ".maxWaitSecs", "1500").toInt,
      app.getProperty( configKey + ".groupEmails", "false").toBoolean,
      app.getProperty( configKey + ".expect", "true").toBoolean )
    this.parent = parent
  }


  def acceptAlert(alert:Alert) : Boolean = synchronized {
    try {
      var accepted = paramMatchers.forall{ _.acceptAlert(alert) }
      if ( !expect ) !accepted else accepted
    } catch {
      case NonFatal(e) => e.printStackTrace(); false
    }
  }


  override def add( alert:Alert ) = {
    alert.parent = this
    parent.synchronized {
      val rtn = super.add(alert)
      parent.notifyAll()
      rtn
    }
  }


  def groupByEmail() = synchronized {

    val alertsByEmail = new java.util.HashMap[String,AlertGroup]()

    for( alert <- this ) {
      alert.withEmails{ email =>
        var alertsForEmail = alertsByEmail.get(email)
        if ( alertsForEmail == null ) {
          alertsForEmail = new AlertGroup(configKey,parent)
          alertsByEmail(email) = alertsForEmail
        }
        alertsForEmail.add(alert)
      }
    }
    alertsByEmail
  }


  def groupByEmails() = synchronized {
    val emailWithAlertsGroupedByAlertHostAndDescSet = groupByEmail().toList.groupBy( _._2.map{ alert => (alert.hostName, alert.desc ) }.toSet )
    emailWithAlertsGroupedByAlertHostAndDescSet.map{ case (_,emailWithAlertsList:List[(String,AlertGroup)]) => (emailWithAlertsList.map{_._1}.toSet,emailWithAlertsList.head._2)}
  }


  def groupByTypeAndState() = this.groupBy { n => (n.notifyType, n.state)}


  def groupByTypeAndStateAsList() = groupByTypeAndState().toList.sortBy{g=>g._1}


  def isPastDue = size() > 0 && nextDueMs <= 0


  def nextDueMs : Long = synchronized {

    if ( isEmpty ) {
      return 0
    }

    val now = System.currentTimeMillis()
    val elapsedMs = now - this.head.addTimeMs
    val maxWaitMs = maxWaitSecs * 1000
    val maxExpireMs = maxWaitMs - elapsedMs

    return if ( maxExpireMs > 0 ) {
      val idleElapsedMs = now - this.last.addTimeMs
      val idleWaitMs = idleWaitSecs * 1000
      Math.min(maxExpireMs,idleWaitMs - idleElapsedMs)
    } else {
      maxExpireMs
    }
  }


  def sendEmails(): Unit = synchronized {

    for( alert <- this ) {
      alert.remove()
    }

    if (groupEmails) {
      for( (emails,alerts) <- groupByEmails() ) {
        EmailThread.instance.put(emails,alerts)
      }
    } else {
      for( (email,alerts) <- groupByEmail() ) {
        EmailThread.instance.put(email,alerts)
      }
    }

    clear()
  }


  override def toString = {
    s"Config Key: $configKey, Title: $title, Idle Wait Secs: $idleWaitSecs, Max Wait Secs: $maxWaitSecs, Expect: $expect, Parameter Matchers: $paramMatchers"
  }


  def withAlerts( fn: (Alert) => Unit ) = synchronized{ for( alert <- this ) alert.synchronized{ fn (alert) } }

}
