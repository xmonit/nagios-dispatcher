package com.xmonit.dispatcher.nagios



object SvcAlert {

  val alertsByHostAndDesc = java.util.Collections.synchronizedMap(new java.util.LinkedHashMap[(String,String),SvcAlert]())

  def state(params:Map[String,String]) = Alert.getParam(params,"SERVICESTATE")

  def timeStamp(params:Map[String,String]) = Alert.getParam(params,"LASTSERVICECHECK")

  def output(params:Map[String,String]) = Alert.getParam(params,"SERVICEOUTPUT")

  def key(params:Map[String,String]) = ( state(params), timeStamp(params), output(params) )

}



class SvcAlert( svcParams: Map[String,String],
                addTimeMs: Long = System.currentTimeMillis() ) extends Alert ( svcParams, addTimeMs ){

  override def actionUrl =  getActionUrl("SERVICEACTIONURL")

  override def desc =  getParam("SERVICEDESC")

  override def key(params:Map[String,String]) = SvcAlert.key(params)

  override def output = SvcAlert.output(params)

  override def notesUrl = getParamOrElse("SERVICENOTESURL","")

  override def state = SvcAlert.state(params)

  override def timeStamp = SvcAlert.timeStamp(params)

  override def remove() = SvcAlert.alertsByHostAndDesc.remove((hostName,desc))

  override def toString = {
    state + " " +  hostName + " " + desc + " " + output
  }

}


