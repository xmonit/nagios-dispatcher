package com.xmonit.dispatcher.nagios

import java.io.File
import java.util.concurrent.atomic.AtomicBoolean

import org.fusesource.scalate.TemplateEngine
import org.slf4j.LoggerFactory
import com.xmonit.dispatcher.AppConfig.{instance => app}

import scala.collection.JavaConversions._

object AlertGroupManager extends Thread {

  final val Log = LoggerFactory.getLogger(classOf[AlertGroupManager])

  val hostInstance = new AlertGroupManager("host")
  val svcInstance = new AlertGroupManager("service")

  val templateEngine = new TemplateEngine

  def expandTemplate( alertsGrp: AlertGroup, templatePath: String, templateDir: String = getTemplateDir() ) = synchronized {
    val template = templateEngine.load(new File(templateDir, templatePath))
    templateEngine.layout("", template, Map("alertGroup" -> alertsGrp, "templateDir" -> templateDir ) )
  }

  def initTestData(templateName: String = "testData.ssp", templateDirProp: String = "email.test.template.dir",
                   svcAlertGroup: AlertGroup, hostAlertGroup: AlertGroup) {
    val templateDir = getTemplateDir(templateDirProp)
    val templateFile = new File(templateDir, "/nagiosNotification/" + templateName)
    val testTemplate = templateEngine.load(templateFile)

    templateEngine.layout("", testTemplate, Map(
      "svcAlertGroup" -> (if ( svcAlertGroup != null ) Some(svcAlertGroup) else None),
      "hostAlertGroup" -> (if ( hostAlertGroup != null ) Some(hostAlertGroup) else None)))
  }

  def getTemplateDir(templateDirProp: String = "email.template.dir" ) = {
    var templateDir = app.getProperty(templateDirProp, "etc/templates")
    if (!templateDir.startsWith("/")) {
      // scalate render only works when path starts with forward slash if not URI format (needed during windows debug)
      templateDir = new File(templateDir).getAbsolutePath.replaceAll("^[A-Za-z]:", "").replaceAll( """\\""", "/")
    }
    templateDir
  }

  def init(): Unit = {
    svcInstance.init()
    hostInstance.init()
  }

  def restart(): Unit = {
    signalStop()
    Thread.sleep(2000);
    init()
  }

  def signalStop(): Unit = {
    svcInstance.signalStop()
    hostInstance.signalStop()
  }

}


import AlertGroupManager._

class AlertGroupManager(val alertGroupKey:String) extends java.util.Vector[AlertGroup] with Runnable {

  protected val bStop = new AtomicBoolean(false)


  def init() = synchronized {
    withGroups{ _.sendEmails() }
    clear()
    val alertGroupNames = app.getProperty(alertGroupKey + ".alert.groups").split("""\s*,\s*""")
    for( name <- alertGroupNames ) {
      add( new AlertGroup(name, this) )
    }
    AlertGroupManager.Log.debug(this.toString())
    new Thread(this).start()
  }


  def run() = synchronized {
    bStop.set(false)
    Log.info("Started alert group manager thread: " + alertGroupKey )

    while (!bStop.get) {
      try {
        if (!isEmpty) {
          for (alertGroup <- this) {
            if (alertGroup.isPastDue) {
              alertGroup.sendEmails()
            }
          }
        }
        val nonEmptyGroups = this.toList.filter { !_.isEmpty }
        var nextTimeoutMs = if (nonEmptyGroups.isEmpty) 0L
        else nonEmptyGroups.map { _.nextDueMs }.min
        AlertGroupManager.Log.debug("Waiting " + nextTimeoutMs + " ms")
        wait(nextTimeoutMs)
      } catch {
        case t:Throwable => t.printStackTrace()
      }
    }

    Log.info("Exited main loop of alert group manager thread: " + alertGroupKey )
  }

  def signalStop() = synchronized {
    bStop.set(true)
    notifyAll()
  }

  def withGroups( fn: (AlertGroup) => Unit ) = synchronized{ for( group <- this ) group.synchronized{ fn (group) } }


  def expandTemplates(alertsGrp: AlertGroup, templateDirProp: String = "email.template.dir") = {
    val templateDir = getTemplateDir(templateDirProp)
    val subject = expandTemplate(alertsGrp,"/nagiosNotification/" + alertGroupKey + "Subject.ssp",templateDir)
    val body = expandTemplate(alertsGrp,"/nagiosNotification/" + alertGroupKey + "Body.ssp",templateDir)
    (subject, body)
  }

}
