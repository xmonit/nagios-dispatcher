package com.xmonit.dispatcher.nagios

object NagiosStatus extends Enumeration {
  type NagiosStatus = Value
  val OK, WARNING, CRITICAL, UNKNOWN = Value
}
