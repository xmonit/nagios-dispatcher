package com.xmonit.dispatcher.nagios

import com.xmonit.dispatcher.nagios.NagiosStatus._

case class NagiosResult(status: NagiosStatus = OK, msg: String) {
  override def toString = msg
}