package com.xmonit.dispatcher.nagios



object HostAlert {

  val alertsByHost = java.util.Collections.synchronizedMap(new java.util.LinkedHashMap[String,HostAlert]())

  def state(params:Map[String,String]) = Alert.getParam(params,"HOSTSTATE")

  def timeStamp(params:Map[String,String]) = Alert.getParam(params,"LASTHOSTCHECK")

  def output(params:Map[String,String]) = Alert.getParam(params,"HOSTOUTPUT")

  def key(params:Map[String,String]) = ( state(params), timeStamp(params), output(params) )
}



class HostAlert ( hostParams: Map[String,String],
                  addTimeMs: Long = System.currentTimeMillis() ) extends Alert ( hostParams,addTimeMs ){

  override def actionUrl =  getActionUrl("HOSTACTIONURL")

  override def desc = hostName

  override def key(params:Map[String,String]) = HostAlert.key(params)

  override def output = HostAlert.output(params)

  override def notesUrl = params.getOrElse( Alert.getParamName("HOSTNOTESURL"),"")

  override def state = HostAlert.state(params)

  override def timeStamp = HostAlert.timeStamp(params)

  override def remove() = HostAlert.alertsByHost.remove(hostName)

}
