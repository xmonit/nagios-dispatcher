package com.xmonit.dispatcher.nagios

import scala.util.parsing.combinator._
import java.io.Reader

class NagiosStatusParser extends RegexParsers {
  
  var varNameRegex = """\w+""".r
  
  var lineSepRegEx = """[ \t\r\n]*""".r
  
  def keyValue: Parser[(String, String)] =
    ( varNameRegex <~ "=") ~ """.*""".r <~ lineSepRegEx ^^ {
      case key ~ value => (key, value)
    }
    
  def serviceDef: Parser[NagiosService] = 
    (( """([ \t\r\n]|#[^\n]*\n)*""".r ~> varNameRegex <~ "{" <~ """[ \t\r\n]*""".r) ~ rep(keyValue) <~ "}" <~ lineSepRegEx) ^^ {
    case name ~ keyValues => NagiosService(name, keyValues.toMap)
  }
  
  def parse(reader: Reader) = parseAll(rep(serviceDef), reader)
}


