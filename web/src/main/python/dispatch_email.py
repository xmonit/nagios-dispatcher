#!/usr/bin/env python

import httplib, os, pycurl, sys, traceback, urllib

envParams = {}
    
for k, v in os.environ.items():
  if k.startswith("ICINGA_"):
    envParams[k] = v

alertType = 'service'
if len(sys.argv) == 2:
  alertType = sys.argv[1]
    

if 'DISPATCHER_HOST' in os.environ:
  # Used for testing - to send request to workspace IDE debugger
  dispatcherHost = os.environ['DISPATCHER_HOST']
  dispatcherPort = os.environ['DISPATCHER_PORT']
  dispatcherContext = os.environ['DISPATCHER_CONTEXT']
else:
  dispatcherHost = 'localhost'
  dispatcherPort = '80'
  dispatcherContext = '/dispatcher'

#headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
#conn = httplib.HTTPConnection(dispatcherHost + ":" + dispatcherPort)
#conn.request("POST", dispatcherContext + "/email/" + alertType + "Alert", urllib.urlencode(envParams), headers)
#response = conn.getresponse()
#status = response.status

headers = [ "Content-type:application/x-www-form-urlencoded", "Accept:text/plain" ]
url = 'http://' + dispatcherHost + ':' + str(dispatcherPort) + dispatcherContext + "/email/" + alertType + "Alert"
curl = pycurl.Curl()
curl.setopt(pycurl.HTTPHEADER, headers)
curl.setopt(pycurl.POSTFIELDS, str(urllib.urlencode(envParams)))
curl.setopt(pycurl.POST, 1)
curl.setopt(pycurl.SSL_VERIFYPEER, 0)
curl.setopt(pycurl.SSL_VERIFYHOST, 0)
curl.setopt(pycurl.URL, url)
curl.setopt(pycurl.FAILONERROR, True)
#curl.setopt(pycurl.PROXY, 'http://localhost:8118')
try:
  curl.perform()
  status = curl.getinfo(pycurl.HTTP_CODE)
except pycurl.error, error:
  status = error[0]
curl.close() 

if status != 200:
  sys.exit(1)
      
    
