# nagios-dispatcher

Primarily a web service that catches alerts from Nagios and Icinga and puts them into buckets for sending consolidated emails built from templates.  

It also does JMX checks for Nagios and Icinga since that requires a running JVM.  The web service is written in Scala and uses Scalate templates.  
There is a C++ POCO client designed to be executed as a service check by Nagios and Icinga (for sending jmx or auth calls to web service).
There is a python script named dispatch_email.py which is designed to be triggered by Nagios or Icinga when there is an alert that should be
emailed by the dispatcher web service.  The web front end is Bootstrap3 and Angular 1.x for admin config screen.

The web service uses configurable rules in the nagios-dispatcher.properties file that do several things.

  1. Determine which templates are used for emails (see web/etc/templates)
  
  2. Which category the alert belongs to based on host name, service title, etc...
  
  3. How long to wait for new alerts before sending out a set of alerts in single email (using a max and idle timeout for each group)
